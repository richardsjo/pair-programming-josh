from copy import deepcopy
class game_of_life():
	def __init__(self, n):
		#initialising a grid
		self.grid = []
		self.n = n
		for _ in range(n):
			rows = [0 for i in range(n)]
			self.grid.append(rows)


	def live_or_dive(self,grid, i,j, state): #i vertical, j horizontal
		counter = 0
		for k in range(i-1,i+2):
			for l in range(j-1,j+2):
				if k!=i or l!=j:
					if  k > -1 and k < self.n and l>-1 and l<self.n:
						if grid[k][l] ==1:
							counter +=1
		# if grid[i][j] == 1:
		# 	counter -=1
		if state == 1:
			return False if 2 <= counter <= 3 else True
		else:
			return True if counter ==3 else False



	def initialise_list(self,mylist):
		for element in mylist:
			self.grid[element[0]][element[1]] = 1


	def play_game(self, mylist,generations = 10):
		self.initialise_list(mylist)
		for _ in range(generations):
			for line in self.grid:
				print(line) 
			print('-----------------')
			copied_grid = deepcopy(self.grid)
			for i in range(self.n):
				for j in range(self.n):
					if self.grid[i][j] == 1:
						if self.live_or_dive(copied_grid,i,j,1):
							self.grid[i][j] = 0
					else:
						if self.live_or_dive(copied_grid,i,j,0):
							self.grid[i][j] = 1

		for line in self.grid:
			print(line) 







if __name__ == '__main__':
	starting_points = [[1,2],[3,1],[1,1]]
	a = game_of_life(5)
	a.play_game(starting_points)


